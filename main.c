/*
* Copyright (C) 2016 - [HCI]Mara'akate -- http://dk.toastednet.org
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE
*
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*/

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winerror.h>

#if defined(_MSC_VER) && _MSC_VER < 1400 /* FS: VS2005 Compatibility */
#include <winwrap.h>
#endif

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Prototypes */
int ROM_CheckHeader (void);
int ROM_Size (char *romfile);
int Calculate_ROM_Size (int romsize);

/* Globals */
FILE *f;

int ROM_CheckHeader (void)
{
	int i = 0;
	char header[5];

	for (i = 0; i < 5; i++)
		header[i] = (char)fgetc(f);

	header[5] = '\0';

	if(!strcmp("g GCE", header))
	{
//		printf("Header: %s\n", header);
		return 1;
	}
	else
	{
		return 0;
	}
}

int ROM_Size (char *romfile)
{
	int x = 0;

	f = fopen(romfile, "rb+");

	if(!f)
	{
		printf("Error: Couldn't find %s\n", romfile);
		return 0;
	}

	if(!ROM_CheckHeader())
	{
		printf("Error: Invalid header!  Aborting patching process.\n");
		return 0;
	}

	fseek(f, 0, SEEK_END);
	x = ftell(f);

	printf("Size of ROM: %i bytes\n", x);

	return x;
}

int Calculate_ROM_Size (int romsize)
{
	int overdump = 0;
	int i = 0;

	/* FS: First, let's check and see if it's already a valid size. */
	switch(romsize)
	{
		case 65536:
		case 32768:
		case 16384:
		case 8196:
		case 4096:
			i = romsize;
			printf("ROM is already a valid size of %iKB.  Aborting patching process.\n", i/1024);
			return i;
	}

	if (romsize > 65536) /* FS: No ROMs exist greatar than 64KB.  Also unsure if Vectrex properly supports ROMs larger than this */
	{
		printf("Error: ROM size greater than 64KB!\n");
		return 0;
	}
	else if (romsize > 32768)
	{
		printf("Padding ROM to 64KB\n");
		overdump = 65536 - romsize;
	}
	else if (romsize > 16384)
	{
		printf("Padding ROM to 32KB\n");
		overdump = 32768 - romsize;
	}
	else if (romsize > 8196)
	{
		printf("Padding ROM to 16KB\n");
		overdump = 16384 - romsize;
	}
	else if (romsize > 4096)
	{
		printf("Padding ROM to 8KB\n");
		overdump = 8196 - romsize;
	}
	else
	{
		printf("Padding ROM to 4KB\n");
		overdump = 4096 - romsize;
	}

	for(i = 0; i < overdump; i++)
	{
		if (fputc(0, f) == EOF)
		{
			printf("Error writing to file!\n");
			return i;
		}
	}

	fflush(f);
	printf("Padded %i bytes\n", i);
	return i;
}

int main (int argc, char **argv)
{
	int romsize = 0;

	printf("*** Vectrex ROM Overdumper ***\n");
	printf("*** For use with the Mateos Vectrex Rewritable Multigame Cartridge ***\n");
	printf("Coded by [HCI]Mara'akate -- http://dk.toastednet.org.\n\n");

	if (argc == 2)
	{
		printf("File to process: %s\n", argv[1]);
		romsize = ROM_Size(argv[1]);

		if(romsize)
		{
			Calculate_ROM_Size(romsize);
		}
		fclose(f);
	}
	else
	{
		printf("Usage: vecover.exe game.bin\n");
	}

	return 0;
}
